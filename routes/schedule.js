const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const login = require('../middleware/login');

router.get('/', login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            'SELECT * FROM schedule;',
            ((error, result, field) => {
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    amout: result.lenght,
                    schedule: result.map(event => {
                        return{
                            id_event: schedule.id_event,
                            id_user: schedule.id_user, 
                            reques: {
                                tipo: 'GET',
                                information: 'Retornando todos os eventos e seus participantes',
                                url: 'http://localhost:4000/schedule'
                            }
                        }
                    })
                }
                return res.status(200).send(response);
            })
        )
    })
})



router.post('/', login,(req, res, next) => {
    if(error) {return res.status(500).send({error: error})}
    mysql.getConnection((error, conn) => {
        conn.query(
            `INSERT INTO schedule (id_event, id_user) 
            VALUES (?,?);`,
            [req.body.id_event, req.body.id_user],
            (error, result, field) => {
                conn.release()
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    mensagem: 'Usuario cadastrado com sucesso no evento',
                    createdEvent: {
                        id_event: schedule.id_event,
                        id_user: schedule.id_user, 
                        reques: {
                            tipo: 'POST',
                            information: 'Insere um usuário a um evento',
                            url: 'http://localhost:4000/schedule'
                        }
                    }
                }
                return res.status(201).send(response);
            }
        )
    })
})

router.get('/:id_event', login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            `SELECT email FROM user u
            inner join schedule s
            on u.email = s.id_user
            inner join event e
            on ? = s.id_event;`,
            [req.params.id_event],
            ((error, result, field) => {
                if(result.length == 0){
                    return res.status(404).send({
                        message: 'Não foi encontrado niguém para esse evento'
                    })
                }
                const response = {
                    mensagem: 'Usuarios encontrado com sucesso para esse evento',
                    event: {
                        id_event: schedule.id_event,
                        id_user: schedule.id_user, 
                        request: {
                            type: 'GET',
                            information: 'Retorna os usuarios de um evento',
                            url: 'http://localhost:4000/schedule'
                        }
                    }
                } 
                return res.status(200).send(response);
            })
        )
    })
})

router.patch('/',(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            `UPDATE schedule 
            SET id_user = ?, 
            id_event = ?
            WHERE id_event = ?;`,
            [req.body.id_user, req.body.id_event, req.body.id_user],
            ((error, result, field) => {
                conn.release()
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    mensagem: 'Evento atualizado com sucesso',
                    createdEvent: {
                        id_event: schedule.id_event,
                        id_user: schedule.id_user, 
                        reques: {
                            tipo: 'PATCH',
                            information: 'Atualiza um evento',
                            url: 'http://localhost:4000/schedule/' + user.id_event
                        }
                    }
                }
                return res.status(202).send(response);
            })
        )
    })
})

router.delete('/', (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            `DELETE FROM schedule WHERE id_event = ? AND id_user = ?;`,
            [req.body.id_event, req.body.id_user],
            ((error, result, field) => {
                conn.release()
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    mensagem: 'Evento REMOVIDO com sucesso',
                    createdEvent: {
                        id_event: schedule.id_event,
                        id_user: schedule.id_user, 
                        reques: {
                            tipo: 'DELETE',
                            information: 'Remove um evento',
                            url: 'http://localhost:4000/event'
                        }
                    }
                }
                return res.status(202).send(response);
            })
        )
    })
})

module.exports = router