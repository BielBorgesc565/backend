const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const login = require('../middleware/login');

router.get('/',(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            'SELECT * FROM event;',
            ((error, result, field) => {
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    amout: result.lenght,
                    events: result.map(event => {
                        return{
                            id_event: event.id_event,
                            description: event.description, 
                            init_date: event.init_date,
                            final_date: event.final_date,
                            reques: {
                                tipo: 'GET',
                                information: 'Retornando todos os eventos',
                                url: 'http://localhost:4000/event'
                            }
                        }
                    })
                }
                return res.status(200).send(response);
            })
        )
    })
})



router.post('/', login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            `INSERT INTO event (description, init_date, final_date) 
            VALUES (?, ?, ?);`,
            [req.body.description, req.body.init_date, req.body.final_date],
            (error, result, field) => {
                conn.release()
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    mensagem: 'Evento cadastrado com sucesso',
                    createdEvent: {
                        description: result.description, 
                        init_date: result.init_date,
                        final_date: result.final_date,
                        reques: {
                            tipo: 'POST',
                            information: 'Insere um evento',
                            url: 'http://localhost:4000/event'
                        }
                    }
                }
                return res.status(201).send(response);
            }
        )
    })
})

router.get('/:id_event',login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            'SELECT * FROM event WHERE id_event = ?;',
            [req.params.id_event],
            ((error, result, field) => {
                if(result.length == 0){
                    return res.status(404).send({
                        message: 'Não foi encontrado nenhum evento com esse id'
                    })
                }
                const response = {
                    mensagem: 'Evento encontrado com sucesso',
                    event: {
                        id_event: result[0].id_event,
                        description: result[0].description, 
                        init_date: result[0].init_date,
                        final_date: result[0].final_date,
                        request: {
                            type: 'GET',
                            information: 'Retorna um evento',
                            url: 'http://localhost:4000/event/'
                        }
                    }
                } 
                return res.status(200).send(response);
            })
        )
    })
})

router.patch('/',login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            `UPDATE event 
            SET description = ?, 
            init_date = ?, 
            final_date = ?
            WHERE id_event = ?;`,
            [req.body.description, req.body.init_date, req.body.final_date, req.body.id_event],
            ((error, result, field) => {
                conn.release()
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    mensagem: 'Evento atualizado com sucesso',
                    createdEvent: {
                        id_event: result.id_event,
                        description: result.description, 
                        init_date: result.init_date,
                        final_date: result.final_date,
                        reques: {
                            tipo: 'PATCH',
                            information: 'atualiza um evento',
                            url: 'http://localhost:4000/event/'
                        }
                    }
                }
                return res.status(202).send(response);
            })
        )
    })
})

router.delete('/:id_event', login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            `DELETE FROM event WHERE id_event = ?;`,
            [req.params.id_event],
            ((error, result, field) => {
                conn.release()
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    mensagem: 'Evento REMOVIDO com sucesso',
                    createdEvent: {
                        reques: {
                            tipo: 'DELETE',
                            information: 'Remove um evento',
                            url: 'http://localhost:4000/event'
                        }
                    }
                }
                return res.status(202).send(response);
            })
        )
    })
})

module.exports = router;