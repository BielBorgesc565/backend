const express = require('express')
const router = express.Router()
const mysql = require('../mysql').pool
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const login = require('../middleware/login')


router.get('/',login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            'SELECT * FROM user;',
            ((error, result, field) => {
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    amout: result.lenght,
                    users: result.map(user => {
                        return{
                            email: user.email,
                            password: user.password, 
                            reques: {
                                type: 'GET',
                                information: 'Retornando todos os usuários',
                                url: 'http://localhost:4000/user'
                            }
                        }
                    })
                }
                return res.status(200).send(response);
            })
        )
    })
})

router.post('/create', (req, res, next) => {
    mysql.getConnection((error, conn) =>{
        if(error) {return res.status(500).send({error: error})}
        conn.query('SELECT * FROM user WHERE email = ? ', [req.body.email],(error, result) => {
            if(error) {return res.status(500).send({error: error})}
            if(result.length > 0){
                res.status(401).send({message: 'Usuário já cadastrado'})
            }else{
                bcrypt.hash(req.body.password, 10, (errBcrypt, hash) => {
                    if(errBcrypt) {return res.status(500).send({error: errBcrypt})}
                    conn.query(
                        `INSERT INTO user (email, password) VALUES (?,?)`,
                    [req.body.email, hash],
                    (error, result) => {
                        conn.release()
                        if(error) {return res.status(500).send({error: error})}
                        const response = {
                            message: 'Usuario criado com sucesso',
                            createdUser: {
                                email: req.body.email
                            }
                        }
                        return res.status(201).send(response);
                    })
                })
            }
        })
    })
})

router.post('/login', (req, res, next) => {
    mysql.getConnection((error, conn)=>{
        if(error) {return res.status(500).send({error: error})}
        const query = `SELECT * FROM user WHERE email = ?`
        conn.query(query,[req.body.email],(error, results, fields) => {
            conn.release()
            if(error) {return res.status(500).send({error: error})}
            if(results.length < 1){
                return res.status(401).send({message: 'Falha na autenticação', status: 401});
            }
            bcrypt.compare(req.body.password, results[0].password, (error,result) => {
                if(error){
                    return res.status(401).send({message: 'Falha na autenticação', status: 401});
                }
                if(result){
                    const token = jwt.sign({
                        email: results[0].email
                    }, "!#%&(@$¨*",
                    {
                        expiresIn: "1h"
                    })
                    return res.status(200).send({
                        message: 'Autenticado com sucesso',
                        token: token
                })
                }
                return res.status(401).send({message: 'Falha na autenticação', status: 401});
            })
        })
    })
})

router.get('/:email', login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            'SELECT * FROM user WHERE email = ?;',
            [req.params.email],
            ((error, result, field) => {
                if(error) {return res.status(500).send({error: error})}
                if(result.length == 0){
                    return res.status(404).send({
                        message: 'Não foi encontrado nenhum usuário com esse id'
                    })
                }
                const response = {
                    user: {
                        email: result[0].email,
                        password: result[0].password,
                        request: {
                            type: 'GET',
                            information: 'Retorna um usuário',
                            url: 'http://localhost:4000/user'
                        }
                    }
                } 
                return res.status(200).send(response)
            })
        )
    })
})

router.patch('/', login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            `UPDATE user 
            SET password = ?
            WHERE email = ?;`,
            [req.body.password, req.body.email],
            ((error, result, field) => {
                conn.release()
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    mensagem: 'Usuário atualizado com sucesso',
                    updatedUser: {
                        email: req.body.email,
                        password: req.body.password,
                        request: {
                            type: 'PATCH',
                            information: 'Atualiza um usuário',
                            url: 'http://localhost:4000/user/' + req.body.email
                        }
                    }
                } 
                return res.status(202).send(response);
            })
        )
    })
})

router.delete('/', login,(req, res, next) => {
    mysql.getConnection((error, conn) => {
        if(error) {return res.status(500).send({error: error})}
        conn.query(
            `DELETE FROM user WHERE email = ?;`,
            [req.body.email],
            ((error, result, field) => {
                conn.release()
                if(error) {return res.status(500).send({error: error})}
                const response = {
                    mensagem: 'Usuário removido com sucesso',
                    updatedUser: {
                        email: req.body.email,
                        password: req.body.password,
                        request: {
                            type: 'DELETE',
                            information: 'Remove um usuário',
                            url: 'http://localhost:4000/user'
                        }
                    }
                } 
                return res.status(201).send(response);
            })
        )
    })
})

module.exports = router;