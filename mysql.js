const mysql = require('mysql2');

var pool = mysql.createPool({
    "user" : process.env.MYSQ_USER,
    "password": process.env.MYSQ_PASSWORD,
    "database": process.env.MYSQ_DATABASE,
    "host": process.env.MYSQ_HOST,
    "port": process.env.MYSQ_PORT
})

exports.pool = pool;